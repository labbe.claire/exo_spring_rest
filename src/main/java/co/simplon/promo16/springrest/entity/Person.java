package co.simplon.promo16.springrest.entity;

public class Person {
    private Integer id;
    private String name;
    private String firstname;
    private Integer age;
    
    public Person(String name, String firstname, Integer age) {
        this.name = name;
        this.firstname = firstname;
        this.age = age;
    }
    public Person() {
    }
    public Person(Integer id, String name, String firstname, Integer age) {
        this.id = id;
        this.name = name;
        this.firstname = firstname;
        this.age = age;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
}


DROP TABLE IF EXISTS person;
CREATE TABLE person(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    name VARCHAR(255),
    firstname VARCHAR(255),
    age INT(10)
);

INSERT INTO person (name, firstname, age) VALUES ("Dupont", "Jean", 10);
INSERT INTO person (name, firstname, age) VALUES ("Durand", "Pierre", 20);
INSERT INTO person (name, firstname, age) VALUES ("Duroux", "Cédric", 30);